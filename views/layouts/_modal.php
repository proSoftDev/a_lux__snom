<div class="modal-snom">
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLabel">Задать вопрос?</h1>
                    <p>Задайте вопрос и мы сразу ответим!</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="/public/images/exit-modal.png" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <br>
                    <input type="text" name="Feedback[fio]" placeholder="ФИО">
                    <br>
                    <div class="row">
                        <br>
                        <div class="col-xl-6">
                            <input type="text" name="Feedback[telephone]" placeholder="номер телефона">
                        </div>
                        <div class="col-xl-6">
                            <input type="text" name="Feedback[email]" placeholder="email">
                        </div>
                    </div>
                    <textarea id="" cols="30" rows="7" name="Feedback[content]" placeholder="Вопрос"></textarea>
                    <br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-snow" id="sendFeedback">Оставить заявку</button>
                </div>
            </form>
        </div>
    </div>
</div>



<div class="modal-snom">
    <div class="modal fade" id="tarifModal" tabindex="-1" role="dialog" aria-labelledby="tarifModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLabel">Задать вопрос?</h1>
                    <p>Задайте вопрос и мы сразу ответим!</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true"><img src="/public/images/exit-modal.png" alt=""></span>
                    </button>
                </div>
                <div class="modal-body">
                    <br>
                    <input type="text" name="Feedback[fio]" placeholder="ФИО">
                    <br>
                    <div class="row">
                        <br>
                        <div class="col-xl-6">
                            <input type="text" name="Feedback[telephone]" placeholder="номер телефона">
                        </div>
                        <div class="col-xl-6">
                            <input type="text" name="Feedback[email]" placeholder="email">
                        </div>
                    </div>
                    <textarea id="" cols="30" rows="7" name="Feedback[content]" placeholder="Вопрос"></textarea>
                    <br>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-snow" id="sendFeedback">Оставить заявку</button>
                </div>
            </form>
        </div>
    </div>
</div>




<div class="modal-snom">
    <div class="modal fade" id="requestModal" tabindex="-1" role="dialog" aria-labelledby="requestModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLabel">Заказать звонок</h1>
                    <p>Задайте вопрос и мы сразу ответим!</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" name="Feedback[telephone]" placeholder="ФИО">
                    <div class="row">
                        <br>
                        <div class="col-xl-6">
                            <input type="text" name="Feedback[telephone]" placeholder="номер телефона">
                        </div>
                        <div class="col-xl-6">
                            <input type="text" name="Feedback[email]" placeholder="email">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-snow" id="sendRequest">Оставить заявку</button>
                </div>
            </form>
        </div>
    </div>

</div>




<div class="modal-snom">
    <div class="modal fade" id="bannerModal" tabindex="-1" role="dialog" aria-labelledby="bannerModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <form class="modal-content">
                <input type="hidden" name="<?= Yii::$app->request->csrfParam ?>" value="<?= Yii::$app->request->getCsrfToken() ?>" />
                <div class="modal-header">
                    <h1 class="modal-title" id="exampleModalLabel">Заказать звонок</h1>
                    <p>Задайте вопрос и мы сразу ответим!</p>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" name="Feedback[fio]" placeholder="ФИО">
                    <div class="row">
                        <br>
                        <div class="col-xl-6">
                            <input type="text" name="Feedback[telephone]" placeholder="номер телефона">
                        </div>
                        <div class="col-xl-6">
                            <input type="text" name="Feedback[email]" placeholder="email">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary btn-snow" id="sendBanner">Оставить заявку</button>
                </div>
            </form>
        </div>
    </div>

</div>



<script src="/public/js/feedback.js"></script>