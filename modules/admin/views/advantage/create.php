<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Advantage */

$this->title = 'Создание преимущества';
$this->params['breadcrumbs'][] = ['label' => 'Преимущества', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="advantage-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
