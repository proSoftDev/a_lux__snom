<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\AdminEmail */

$this->title = 'Create Admin Email';
$this->params['breadcrumbs'][] = ['label' => 'Admin Emails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="admin-email-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
